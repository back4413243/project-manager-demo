<?php

namespace App\Exports;

use App\Models\project;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProjectExport implements FromCollection, WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        return Project::with(['manager','tasks'])->get();
    }
    public function map($row): array
    {

       return [
           $row->title,
           $row->description,
           $row->manager->first_name,
           $row->manager->last_name,
           $row->tasks->map(fn($query) => [$query->title,]),
           $row->tasks->map(fn($query) => [$query->description,]),
           ];
    }
    public function headings(): array
    {
        return [
          'project title',
          'project description',
          'manager name',
          'manager last name',
          'tasks title',
          'tasks description',

        ];
    }
}
