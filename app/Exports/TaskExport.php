<?php

namespace App\Exports;

use App\Models\task;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TaskExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return task::with(['users','project'])->get();
    }
    public function map($row): array
    {
        return [
            $row->title,
            $row->description,
            $row->project->title,
            $row->project->description,
            $row->users->map(fn($query) => [$query->first_name, $query->last_name]),

        ];
    }
    public function headings(): array
    {
        return [
            'title','description','project title','project description','users name'
        ];
    }
}
