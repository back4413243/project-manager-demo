<?php

namespace App\Http\Middleware;

use App\Models\Task;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UpdateTask
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        if (Auth::user()->hasRole('admin')){
            return $next($request);
        }

        if (!Auth::user()->hasRole('admin')) {
            if($request->task->assigner!=Auth::user()->id ){
                abort(401,'not authorized');
            }
            if (Carbon::now()->diffInHours($request->task->created_at) < 24) {
                return $next($request);

            }
            abort(422,'The task has been created for more than 24 hours. ');
        }
        abort(401,'not authenticated ');
    }
}
