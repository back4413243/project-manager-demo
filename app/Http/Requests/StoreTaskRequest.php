<?php

namespace App\Http\Requests;


use App\Enums\TaskStatus;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class StoreTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title'      =>'required|string|min:2|max:20',
            'description'=>'string|nullable|min:2|max:50',
            'status'     =>['required',new Enum(TaskStatus::class)],
            'due_date'   =>'nullable|date_format:Y-m-d|after_or_equal:start_date',
            'assigner'   =>'required|exists:users,id',
            'assignee'   =>'nullable|array|exists:users,id'
        ];
    }
    public function messages()
    {
        return [
            'required'=>'this field is required, هذا الحقل مطلوب',
            'due_date.date_format'=>'date form must be YYYY-MM-DD, 09-05-2013 ادخال التاربخ يجب أن يكون من الشكل '
        ];
    }


}
