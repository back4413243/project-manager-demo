<?php

namespace App\Http\Requests;

use App\Enums\TaskStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title'      =>'string|min:2|max:20',
            'description'=>'string|nullable|min:2|max:50',
            'status'     =>[new Enum(TaskStatus::class)],
            'due_date'   =>'nullable|date_format:Y-m-d|after_or_equal:start_date',
            'assigner'   =>'exists:users,id',
            'assignee'   =>'nullable|array|exists:users,id'
        ];
    }
    public function messages()
    {
        return [
            'due_date.date_format'=>'date form must be YYYY-MM-DD, 2013-09-05 ادخال التاربخ يجب أن يكون من الشكل '
        ];
    }
}
