<?php

namespace App\Http\Requests;

use App\Rules\MaxFileNameLength;
use Illuminate\Foundation\Http\FormRequest;

class AddMediaToTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'file' => ['mimetypes:application/pdf',new MaxFileNameLength],
        ];
    }
}
