<?php

namespace App\Http\Requests;

use App\Enums\UserStatus;
use App\Rules\MaxFileNameLength;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Enum;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_name' =>'required|string|min:3|max:10|unique:users,user_name',
            'first_name'=>'required|string|min:3|max:15',
            'last_name' =>'required|string|min:3|max:10',
            'is_admin'    =>['required',new Enum(UserStatus::class)],
            'password'  =>'required|string|confirmed',
            'image'     =>['nullable','file', 'mimetypes:image/jpeg,image/png','max:10240',new MaxFileNameLength],
        ];
    }
    public function validated($key = null, $default = null)
    {
        return [
            'user_name' =>$this->user_name,
            'first_name'=>$this->first_name,
            'last_name' =>$this->last_name,
            'is_admin'    =>$this->is_admin,
            'password'  =>Hash::make($this->password),

        ];
    }
}
