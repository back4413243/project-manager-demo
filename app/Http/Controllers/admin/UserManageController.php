<?php

namespace App\Http\Controllers\admin;

use App\Enums\UserStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserManageController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth:sanctum');
//        $this->middleware('role:admin');


    }

    public function index()
         {
            $user=User::query()->with('media')->where('is_admin',UserStatus::User)->get();
            return response()->json(['user'=>$user]);
         }
    public function store(AddUserRequest $request)
        {
            $user=User::query()->create($request->validated());
            if($request->is_admin==UserStatus::Admin){
                $role= Role::findByName('admin','users');
                $user->assignRole($role);
            }
            if ($request->hasFile('image')) {
            $user->addMediaFromRequest('image')->toMediaCollection('users');
            }
            return response()->json('user has added successfully');

        }
    public function update(UpdateUserRequest $request,User $user)
         {
            $user->update($request->validated());
             if($request->is_admin==UserStatus::Admin){
                 $role= Role::findByName('admin','users');
                 $user->assignRole($role);
                 }
            if ($request->hasFile('image')) {
            $user->addMediaFromRequest('image')->toMediaCollection('users');
             }
            return response()->json('user has updated successfully');
         }

     public function destroy(User $user)
        {
            $user->delete();
            return response()->json('user has deleted successfully');
        }
     public function assignAdmin(User $user)
        {
            $role = Role::findByName('admin','users');
            $user->update(['is_admin'=>UserStatus::Admin]);
            $user->assignRole($role);
            return response()->json('role has assigned successfully');
        }
}
