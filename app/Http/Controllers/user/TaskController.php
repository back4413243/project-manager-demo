<?php

namespace App\Http\Controllers\user;


use App\Exports\TaskExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddMediaToTaskRequest;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class TaskController extends Controller
{
            public function __construct()
    {
//        $this->middleware('auth:sanctum');
//        $this->middleware('update_task',['only'=>'update']);

    }

    public function index()
    {
        if (auth()->user()->hasRole('admin')) {
            $task=Task::query()->with(['assigner','users','project']);
        }
        else{$task=Task::query()->with(['assigner','users','project'])->
        where('assigner',Auth::id())->orWhereHas('users',function ($query){
            $query->where('assignee',Auth::id());
        });
        }
        if (!$task){
            abort(404,'No project found');
        }


      $task= QueryBuilder::for($task)->allowedFilters([
               'title',
               AllowedFilter::callback('assigner', function ($query, $value) {
                   $query->whereHas('assigner', function ($query) use ($value) {
                       $query->where('user_name', 'like', $value . '%');
                   });
               }),
                   AllowedFilter::callback('project', function ($query, $value) {
                   $query->whereHas('project', function ($query) use ($value) {
                       $query->where('title', 'like', $value . '%');
                   });
               }),
               AllowedFilter::callback('users', function ($query, $value) {
                   $query->whereHas('users', function ($query) use ($value) {
                       $query->where('user_name', 'like', $value . '%');
                   });
               }),
           ])->paginate(5)->appends(request()->query());
      return response()->json(['tasks'=>$task]);
    }



    public function store(StoreTaskRequest $request)
    {
        $task=Task::query()->create($request->validated());
        if($request->assignee){
        $task->users()->attach($request->assignee);
        }
        return response()->json(['task'=>$task]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {

        $task->load('users','project','comments','media');
        return response()->json(['tasks'=>$task]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {

        $task->update($request->validated());
        if($request->assignee) {
            $task->users()->sync($request->assignee);
        }
        return response()->json(['task'=>$task]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        if(!$this->authorize('delete', $task)){
           abort(422,'task has status done');
        }
        $task->delete();
        return response()->json('task has been deleted');

    }
    public function uploadFile(AddMediaToTaskRequest $request,Task $task)
    {
        $task->addMediaFromRequest('file')->toMediaCollection('task');
    }
    public function export()
    {
        return Excel::download(new TaskExport(), 'users.xlsx');
    }
}
