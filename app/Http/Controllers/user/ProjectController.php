<?php

namespace App\Http\Controllers\user;

use App\Exports\ProjectExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\InviteMemberRequest;
use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Models\Project;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (auth()->user()->hasRole('admin')) {
            $project=Project::query()->with(['manager', 'tasks', 'users']);
                }
        else{$project=Project::query()->with(['manager','users','tasks'])->
        where('user_id',Auth::id())->orWhereHas('users',function ($query){
             $query->where('member',Auth::id());
        });
        }
        if (!$project){
            abort(404,'No project found');
        }
            $projects = QueryBuilder::for($project)
                ->allowedFilters([
                    'title',
                    AllowedFilter::callback('tasks', function ($query, $value) {
                        $query->whereHas('tasks', function ($query) use ($value) {
                            $query->where('title', 'like','%'. $value . '%');
                        });
                    }),
                ])->paginate(5)->appends(request()->query());

       return response()->json(['projects'=>$projects]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProjectRequest $request)
    {
      $project= Project::query()->create($request->validated());
        if($request->member){
            $project->users()->attach($request->member);
        }
      return response()->json($project);
    }

    /**
     * Display the specified resource.
     */
    public function show(Project $project)
    {
        $project->load('manager','tasks','users');
        return response()->json(['projects'=>$project]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {
        $project->update($request->validated());
        if($request->member) {
            $project->users()->sync($request->member);
        }
        return response()->json(['project'=>$project]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {
        if(!$this->authorize('delete', $project)){
        abort(422,'project has tasks');
        }
        $project->delete();
        return response()->json('project has been deleted');
    }
    public function export()
    {
        return Excel::download(new ProjectExport(), 'users.xlsx');
    }
    public function inviteMember(Project $project,InviteMemberRequest $request)
    {
        if(!$this->authorize('inviteMember', $project)){
            throw new AuthorizationException('not authorized',403);
        }
            $project->users()->sync($request->member);
        return response()->json(['member has invited successfully']);
    }
}
