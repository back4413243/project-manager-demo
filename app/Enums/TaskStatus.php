<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;
use PhpParser\Node\Expr\Cast\Bool_;

/**
 * @method static static ToDo()
 * @method static static InProgress()
 * @method static static Done()

 */
Enum TaskStatus:int
{
    case ToDo=0;
    case InProgress=1;
    case Done=2;

    public function isToDo(): Bool
    {
        return $this == self::ToDo;
    }
    public function isInProgress(): Bool
    {
        return $this == self::InProgress;
    }
    public function isDone(): Bool
    {
        return $this == self::Done;
    }
}


