<?php

namespace App\Models;

use App\Enums\TaskStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\Comment\Entities\Comment;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Task extends Model implements HasMedia
{
    use HasFactory,InteractsWithMedia;

    protected $guarded=['id'];

    protected $casts=['status'=>TaskStatus::class];


    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('task');
    }


    //relations
    public function assigner(): BelongsTo
    {
        return $this->belongsTo(User::class,'assigner');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class,
            'task_user','task_id','assignee');
    }
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class,'commentable');
    }

}
