<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Project extends Model
{
    use HasFactory;

    protected $guarded=['id'];


    //Relations
    public function manager(): BelongsTo
    {
       return $this->belongsTo(User::class,'user_id');
    }
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class,
            'project_user','project_id','member');
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class);
    }



}
