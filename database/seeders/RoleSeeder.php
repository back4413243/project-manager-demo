<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
                Permission::create(['guard_name' => 'users','name' => 'manage_users']);
                Permission::create(['guard_name' => 'users','name' => 'manage_projects']);
                Permission::create(['guard_name' => 'users','name' => 'manage_tasks']);
        $role = Role::create(['guard_name' => 'users','name' => 'admin']);
        $role->givePermissionTo('manage_users');
        $role->givePermissionTo('manage_projects');
        $role->givePermissionTo('manage_tasks');

        $user= User::create([
            'first_name'     => 'Ra.ed',
            'last_name'      => 'Katta.a',
            'user_name'      => 'Ra.edKatta.a35',
            'is_admin'       => 1,
            'password'       => Hash::make('12345678'),

        ]);
        $user->assignRole($role);

//        Permission::create(['name' => 'chat']);
//        Permission::create(['name' => 'edit articles']);
//        Permission::create(['name' => 'delete articles']);
//
//
//        // create roles and assign existing permissions
//
//        $role4=Role::create(['guard_name' => 'api','name'=>'patient']);
////        $role1->givePermissionTo('edit articles');
////        $role1->givePermissionTo('delete articles');
//        $role1->givePermissionTo('chats');
//        $role4->givePermissionTo('chat');//        Permission::create(['guard_name' => 'doctor','name' => 'chats']);
////        Permission::create(['name' => 'chat']);
////        Permission::create(['name' => 'edit articles']);
////        Permission::create(['name' => 'delete articles']);
////
////
////        // create roles and assign existing permissions
////        $role1 = Role::create(['guard_name' => 'doctor','name' => 'doctor']);
////        $role4=Role::create(['guard_name' => 'api','name'=>'patient']);
//////        $role1->givePermissionTo('edit articles');
//////        $role1->givePermissionTo('delete articles');
////        $role1->givePermissionTo('chats');
////        $role4->givePermissionTo('chat');


        //        $doctor->assignRole($role3);

    }
}
