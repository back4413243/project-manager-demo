<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $projects=Project::factory(10)->create();
        foreach ($projects as $project)
        {
            $user=User::inRandomOrder()->take(rand(1,3))->pluck('id');
            $project->users()->attach($user);
        }
    }
}
