<?php

namespace Database\Seeders;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tasks=Task::factory(10)->create();
        foreach ($tasks as $task)
        {
            $user=User::inRandomOrder()->take(rand(1,3))->pluck('id');
            $task->users()->attach($user);
        }
    }
}
