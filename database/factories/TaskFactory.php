<?php

namespace Database\Factories;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        return [
            'title'=>$this->faker->title,
            'description'=>$this->faker->text,
            'assigner'=>User::query()->inRandomOrder()->first()->id,
            'project_id'=>Project::query()->inRandomOrder()->first()->id,
            'status'=>$this->faker->numberBetween(0,2),
            'due_date'=>$this->faker->date,
        ];
    }
}
