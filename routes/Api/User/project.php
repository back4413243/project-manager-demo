<?php

use App\Http\Controllers\user\ProjectController;
use Illuminate\Support\Facades\Route;

Route::get('project/export',[ProjectController::class,'export']);
Route::post('project/{project}/invite_member',[ProjectController::class,'inviteMember']);
Route::apiResource('project',ProjectController::class);
