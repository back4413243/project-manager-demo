<?php


use App\Http\Controllers\user\TaskController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'task'],function (){
    Route::get('index',[TaskController::class,'index']);
    Route::get('{task}/show',[TaskController::class,'show']);
   Route::post('store',[TaskController::class,'store']);
   Route::post('{task}/update',[TaskController::class,'update']);
   Route::delete('{task}/destroy',[TaskController::class,'destroy']);
   Route::post('{task}/upload_file',[TaskController::class,'uploadFile']);
    Route::get('export',[TaskController::class,'export']);
});
