<?php


use App\Http\Controllers\user\AuthController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'auth'],function (){
   Route::post('login',[AuthController::class,'login']);
   Route::get('logout',[AuthController::class,'logout']);
   Route::post('change_password',[AuthController::class,'changePassword']);
   Route::get('show',[AuthController::class,'show']);
});
