<?php

use App\Http\Controllers\admin\UserManageController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'users'], function(){
   Route::get('index',[UserManageController::class,'index']);
   Route::post('store',[UserManageController::class,'store']);
   Route::post('{user}/update',[UserManageController::class,'update']);
   Route::delete('{user}/destroy',[UserManageController::class,'destroy']);
   Route::post('{user}/assign_admin',[UserManageController::class,'assignAdmin']);
});
