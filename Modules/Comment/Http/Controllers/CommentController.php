<?php

namespace Modules\Comment\Http\Controllers;

use App\Models\Task;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Comment\Entities\Comment;
use Modules\Comment\Http\Requests\CommentRequest;
use Modules\Comment\Http\Requests\UpdateCommentRequest;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
        $this->middleware('role:admin',['only'=>'destroy']);
    }

    public function comment(CommentRequest $request,Task $task)
        {
           $task->comments()->create($request->validated());
           return response()->json('your comment has posted successfully');
        }
     public function updateComment(UpdateCommentRequest $request,Comment $comment)
       {
           $comment->update($request->validated());
           return response()->json('your comment has updated successfully');
       }
     public function destroy(Comment $comment)
     {
         $comment->delete();
         return response()->json('you have deleted the comment');
     }

}
